package com.gorand.test.pattern.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.BaseColumns;

public final class RepoContract {

    private RepoContract() {
    }

    public static final Uri AUTHORITY_URI = Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + RestProvider.AUTHORITY);

    public interface ReposColumns {

        public static final String ID             = "id";
        public static final String NAME           = "RepoName";
        public static final String DESCR          = "Description";
        public static final String URL            = "RepoUrl";
        public static final String FORK           = "Fork";
        public static final String OWNER_LOGIN    = "OwnersLogin";
        public static final String OWNER_URL      = "OwnerUrl";

        public static final String FIELDS_REPOS[] = { BaseColumns._ID, ID, NAME, DESCR, OWNER_LOGIN, FORK, URL, OWNER_URL };

    }

    public static final class Repositories implements BaseColumns, ReposColumns {

        public static final String CONTENT_PATH = "repositories";
        public static final String TABLE_NAME   = "repositories";

        public static final Uri    CONTENT_URI  = Uri.withAppendedPath(AUTHORITY_URI, CONTENT_PATH);
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + RestProvider.AUTHORITY + "." + CONTENT_PATH;

    }

    public static void CREATE(SQLiteDatabase db) {

        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + Repositories.TABLE_NAME + " ( ");
        sb.append(Repositories._ID + " integer primary key autoincrement, ");
        sb.append(Repositories.ID + " integer, ");
        sb.append(Repositories.NAME + " text, ");
        sb.append(Repositories.DESCR + " text, ");
        sb.append(Repositories.URL + " text, ");
        sb.append(Repositories.FORK + " integer, ");
        sb.append(Repositories.OWNER_LOGIN + " text, ");
        sb.append(Repositories.OWNER_URL + " text );");

        db.execSQL(sb.toString());

    }

    public static void INSERT(SQLiteDatabase db, ContentValues[] values) {

        if (db != null) {

            StringBuilder sb = new StringBuilder();
            sb.append("REPLACE INTO " + Repositories.TABLE_NAME + " (");
            sb.append(Utils.join(ReposColumns.FIELDS_REPOS));
            sb.append(") values (");
            sb.append("(SELECT _id from " + Repositories.TABLE_NAME + " WHERE id=?)");
            sb.append(Utils.repeat(",?", 7));
            sb.append(");");

            SQLiteStatement stmt = db.compileStatement(sb.toString());

            db.beginTransaction();

            for (int i = 0; i < values.length; i++) {

                ContentValues value = values[i];

                stmt.bindLong(1, value.getAsLong(Repositories.ID));
                stmt.bindLong(2, value.getAsLong(Repositories.ID));
                stmt.bindString(3, value.getAsString(Repositories.NAME));
                stmt.bindString(4, value.getAsString(Repositories.DESCR));
                stmt.bindLong(5, value.getAsLong(Repositories.FORK));
                stmt.bindString(6, value.getAsString(Repositories.OWNER_LOGIN));
                stmt.bindString(7, value.getAsString(Repositories.OWNER_URL));
                stmt.bindString(8, value.getAsString(Repositories.URL));

                stmt.execute();

            }

            db.setTransactionSuccessful();
            db.endTransaction();

        }

    }

 

}
