package com.gorand.test.pattern.a;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.util.Log;

public class CustomCursorLoader extends CursorLoader {

    public static final String TAG = Consts.TAG;

    public CustomCursorLoader(Context context) {
        super(context);
        Log.i(TAG, "CustomCursorLoader.1");
    }

    public CustomCursorLoader(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
        Log.i(TAG, "CustomCursorLoader.2");
    }

    @Override
    public void deliverResult(Cursor cursor) {

        String msg = (cursor != null) ? "cursor count = " + cursor.getCount() : " cursor = null";
        Log.i(TAG, "deliverResult" + msg);
        super.deliverResult(cursor);
    }

    @Override
    public Cursor loadInBackground() {
        Log.i(TAG, "loadInBackground");
        return super.loadInBackground();
    }

    @Override
    protected void onReset() {
        Log.i(TAG, "onReset");
        super.onReset();
    }

    @Override
    protected void onStartLoading() {
        Log.i(TAG, "onStartLoading");
        super.onStartLoading();
    }

    @Override
    protected void onStopLoading() {
        Log.i(TAG, "onStopLoading");
        super.onStopLoading();
    }

    @Override
    protected void onForceLoad() {
        Log.i(TAG, "onForceLoad");
        super.onForceLoad();
    }

    @Override
    protected Cursor onLoadInBackground() {
        Log.i(TAG, "onLoadInBackground");
        return super.onLoadInBackground();
    }

    @Override
    public void forceLoad() {
        Log.i(TAG, "forceLoad");
        super.forceLoad();
    }

}
