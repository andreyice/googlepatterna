package com.gorand.test.pattern.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ListView;

import com.gorand.test.pattern.a.RepoContract.Repositories;

public class MainActivity extends FragmentActivity implements SwipeRefreshLayoutBottom.OnRefreshListener {

    private static final String      TAG                 = Consts.TAG;
    private static final int         LOADER_ID           = 12345;

    private int                      mPage               = 1;
    private boolean                  mFastScroll;

    private ListView                 mListView;
    private SimpleCursorAdapter      mAdapter;

    private static final String[]    PROJECTION          = { Repositories._ID, Repositories.NAME, Repositories.DESCR };
    private static final int[]       VIEWS_IDS           = { R.id.id_text_view, R.id.user_name_text_view, R.id.body_text_view };

    private SwipeRefreshLayoutBottom mSwipeRefreshLayout;

    private LoaderCallbacks<Cursor>  mLoaderCallbacks    = new LoaderCallbacks<Cursor>() {

                                                             @Override
                                                             public Loader<Cursor> onCreateLoader(int loaderId, Bundle bundle) {
                                                                 Log.e(TAG, "onCreateLoader");

                                                                 Loader<Cursor> loader = null;

                                                                 if (loaderId == LOADER_ID) {
                                                                     loader = new CustomCursorLoader(MainActivity.this, Repositories.CONTENT_URI, PROJECTION,
                                                                             null, null, null);
                                                                 }

                                                                 return loader;
                                                             }

                                                             @Override
                                                             public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
                                                                 Log.e(TAG, "onLoadFinished");

                                                                 mAdapter.swapCursor(cursor);
                                                                 if (!mFastScroll) {
                                                                     mListView.smoothScrollToPosition(cursor.getCount());
                                                                 }

                                                                 if (cursor.getCount() == 0) {
                                                                     update();
                                                                 }
                                                             }

                                                             @Override
                                                             public void onLoaderReset(Loader<Cursor> loader) {
                                                                 Log.e(TAG, "onLoaderReset");
                                                                 mAdapter.swapCursor(null);
                                                             }
                                                         };

    private BroadcastReceiver        mReqComleteReceiver = new BroadcastReceiver() {

                                                             @Override
                                                             public void onReceive(Context context, Intent intent) {

                                                                 mFastScroll = false;

                                                                 boolean is_ok = intent.getBooleanExtra(Consts.INTENT_KEY_RESULT_OK, false);
                                                                 Log.w(TAG, "onReceive  OK = " + is_ok);

                                                                 if (is_ok) {
                                                                     int page = intent.getIntExtra(Consts.NEXT_PAGE, mPage + 1);
                                                                     Log.w(TAG, "onReceive  Page = " + page);
                                                                     mPage = page;

                                                                     boolean has_next = intent.getBooleanExtra(Consts.HAS_NEXT, true);

                                                                     if (!has_next) {
                                                                         mSwipeRefreshLayout.setEnabled(false);
                                                                     }

                                                                 }

                                                                 mSwipeRefreshLayout.setRefreshing(false);

                                                             }
                                                         };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Consts.PAGE, mPage);
        outState.putParcelable("LIST", mListView.onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);

        if (savedInstanceState != null) {
            mPage = savedInstanceState.getInt(Consts.PAGE, 1);
            mFastScroll = true;
        }

        ViewGroup root = (ViewGroup) findViewById(R.id.root_layout);

        mSwipeRefreshLayout = new SwipeRefreshLayoutBottom(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mListView = new ListView(this);
        mSwipeRefreshLayout.addView(mListView);

        root.addView(mSwipeRefreshLayout);

        mAdapter = new SimpleCursorAdapter(this, R.layout.list_item_layout, null, PROJECTION, VIEWS_IDS, 0);

        mListView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mListView.onRestoreInstanceState(savedInstanceState.getParcelable("LIST"));
        } else {
            update();
        }

        getSupportLoaderManager().initLoader(LOADER_ID, null, mLoaderCallbacks);



    }

    @Override
    protected void onStart() {

        IntentFilter filter = new IntentFilter(Consts.ACTION_REQUEST_COMPLETED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReqComleteReceiver, filter);

        super.onStart();
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReqComleteReceiver);
        super.onStop();
    }

    @Override
    public void onRefresh() {

        mSwipeRefreshLayout.setRefreshing(true);

        Log.e(TAG, " onRefresh ");

        update();

    }

    private void update() {

        Intent intent = new Intent(this, RestService.class);
        intent.putExtra(Consts.PAGE, mPage);
        startService(intent);

    }

}
