package com.gorand.test.pattern.a;


public class Utils {
    
    
    public static String repeat(String pattern, int count) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < count; i++) {
            sb.append(pattern);
        }

        return sb.toString();
    }

    public static String join(String[] array) {

        StringBuilder sb = new StringBuilder();

        String separator = "";
        for (String str : array) {
            sb.append(separator);
            separator = ",";
            sb.append(str);
        }

        return sb.toString();

    }

}
