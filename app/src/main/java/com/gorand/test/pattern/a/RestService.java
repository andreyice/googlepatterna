package com.gorand.test.pattern.a;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import com.gorand.test.pattern.a.RepoContract.Repositories;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class RestService extends IntentService {

    private static final String TAG = Consts.TAG;

    public RestService() {
        super(RestService.class.toString());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.w(TAG, "onHandleIntent.start");

        int page = intent.getIntExtra(Consts.PAGE, 1);
        Intent result = new Intent(Consts.ACTION_REQUEST_COMPLETED);

        try {

            String strUrl = Consts.URL + "?" + Consts.PAGE + "=" + page + "&" + Consts.PER_PAGE_STR + "=10&" + Consts.ACCESS_TOKEN;

            URL url = new URL(strUrl);
            URLConnection urlConnection = url.openConnection();
            InputStream in = urlConnection.getInputStream();

            StringBuilder sb = new StringBuilder();

            BufferedReader bReader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = bReader.readLine()) != null) {
                sb.append(line);
            }

            JSONArray reposJson = new JSONArray(sb.toString());
            ContentValues[] repoValues = new ContentValues[reposJson.length()];

            for (int i = 0; i < reposJson.length(); ++i) {
                ContentValues item = new ContentValues();

                JSONObject json_repo_obj = reposJson.getJSONObject(i);
                JSONObject json_owner_obj = json_repo_obj.getJSONObject("owner");

                item.put(Repositories.ID, json_repo_obj.getString("id"));
                item.put(Repositories.NAME, json_repo_obj.getString("name"));
                item.put(Repositories.DESCR, json_repo_obj.getString("description"));
                item.put(Repositories.FORK, json_repo_obj.optBoolean("fork", false) ? 1 : 0);
                item.put(Repositories.OWNER_LOGIN, json_owner_obj.getString("login"));
                item.put(Repositories.OWNER_URL, json_owner_obj.getString("html_url"));
                item.put(Repositories.URL, json_repo_obj.getString("html_url"));

                repoValues[i] = item;
            }

            LocalBroadcastManager.getInstance(this).sendBroadcast(result);
            result.putExtra(Consts.INTENT_KEY_RESULT_OK, true);
            result.putExtra(Consts.NEXT_PAGE, page + 1);
            result.putExtra(Consts.HAS_NEXT, (repoValues.length == 10));

            getContentResolver().bulkInsert(RepoContract.Repositories.CONTENT_URI, repoValues);

        } catch (Exception e) {
            e.printStackTrace();
            result.putExtra(Consts.INTENT_KEY_RESULT_OK, false);
            result.putExtra(Consts.NEXT_PAGE, page);
            LocalBroadcastManager.getInstance(this).sendBroadcast(result);
        }

        Log.w(TAG, "onHandleIntent.end");

    }

}
