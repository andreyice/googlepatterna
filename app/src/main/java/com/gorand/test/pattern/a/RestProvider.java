package com.gorand.test.pattern.a;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import com.gorand.test.pattern.a.RepoContract.Repositories;

public class RestProvider extends ContentProvider {

    public static final String      AUTHORITY  = "com.gorand.test";

    private static final String     TAG        = Consts.TAG;

    private static final UriMatcher sUriMatcher;

    private static final int        PATH_ROOT  = 0;
    private static final int        PATH_REPOS = 1;

    static {
        sUriMatcher = new UriMatcher(PATH_ROOT);
        sUriMatcher.addURI(AUTHORITY, RepoContract.Repositories.CONTENT_PATH, PATH_REPOS);
    }

    private DatabaseHeloper         mDatabaseHelper;

    class DatabaseHeloper extends SQLiteOpenHelper {

        public DatabaseHeloper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            RepoContract.CREATE(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

    }

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new DatabaseHeloper(getContext(), null, null, 1);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        switch (sUriMatcher.match(uri)) {
            case PATH_REPOS: {
                Cursor cursor = mDatabaseHelper.getReadableDatabase().query(Repositories.TABLE_NAME, projection, selection, selectionArgs, null, null,
                        sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), RepoContract.Repositories.CONTENT_URI);
                return cursor;
            }
            default:
                return null;
        }
    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case PATH_REPOS:
                return RepoContract.Repositories.CONTENT_TYPE;
            default:
                return null;
        }
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {

        Log.d(TAG, "bulkInsert " + uri + " values " + values.length);

        switch (sUriMatcher.match(uri)) {
            case PATH_REPOS: {

                RepoContract.INSERT(mDatabaseHelper.getWritableDatabase(), values);
                getContext().getContentResolver().notifyChange(RepoContract.Repositories.CONTENT_URI, null);

                return values.length;
            }

        }

        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (sUriMatcher.match(uri)) {
            case PATH_REPOS: {
                mDatabaseHelper.getWritableDatabase().insert(Repositories.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(RepoContract.Repositories.CONTENT_URI, null);
            }
            default:
                return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        switch (sUriMatcher.match(uri)) {
            case PATH_REPOS:
                return mDatabaseHelper.getWritableDatabase().delete(Repositories.TABLE_NAME, selection, selectionArgs);
            default:
                return 0;
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (sUriMatcher.match(uri)) {
            case PATH_REPOS:
                return mDatabaseHelper.getWritableDatabase().update(Repositories.TABLE_NAME, values, selection, selectionArgs);
            default:
                return 0;
        }
    }

}
