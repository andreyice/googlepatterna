package com.gorand.test.pattern.a;

public class Consts {

    public static final String  TAG                      = "ANDREY";

    public static final int     PER_PAGE                 = 10;

    public static final String  URL                      = "https://api.github.com/users/xing/repos";

    public static final String  PAGE                     = "page";
    public static final String  NEXT_PAGE                = "next_page";
    public static final String  HAS_NEXT                 = "has_next";
    public static final String  PER_PAGE_STR             = "per_page";

    private static final String ACCESS_PARAM             = "access_token";
    private static final String ACCESS_VAL               = "eb7d7f050bc00d806594b3d5fd0a3bd05c7f6efe";

    public static final String  ACCESS_TOKEN             = ACCESS_PARAM + "=" + ACCESS_VAL;

    public static final String  INTENT_KEY_RESULT_OK     = "intent.key.result.ok";
    public static final String  ACTION_REQUEST_COMPLETED = "com.gorand.test.pattern.action.request.completed";

}
